package problem;


import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import tester.Tester;

public class UniformSample {
	private static List<Obstacle> obstacles;
	
	private static ArmConfig initial;
	private static ArmConfig goal;
	
	private int jointnumber;
	private Tester test;
	private static boolean gripper;
	
	private static ArrayList<ArmConfig> path;
	private ArrayList<ArmConfig> map;
	
	private PriorityQueue<Node> queue;
	private Set<ArmConfig> expanded;
	private ArrayList<Double> defaultGrip;
	
	
	UniformSample(ProblemSpec problem){
		queue = new PriorityQueue<>(new Node());
		obstacles = problem.getObstacles();
		initial = problem.getInitialState();
		
		goal = problem.getGoalState();
		
		jointnumber = problem.getJointCount();
		test = new Tester();
		gripper = problem.getGoalState().hasGripper();
		path = new ArrayList<ArmConfig>();
		map = new ArrayList<ArmConfig>();
		expanded = new HashSet<ArmConfig>();
		
		////
		defaultGrip = new ArrayList<Double>();
		defaultGrip.add(0.03);
		defaultGrip.add(0.03);
		defaultGrip.add(0.03);
		defaultGrip.add(0.03);
		
		
		if(gripper){
			Random  r = new Random();
			for(int i= 0; i<10000;i++){
				double x= r.nextDouble();
				double y= r.nextDouble();
				Point2D point = new Point2D.Double();
				point.setLocation(x, y);
				Point2D dia = new Point2D.Double();
				dia.setLocation(x+0.02, y+0.02);
				Rectangle2D chair = new Rectangle2D.Double();
				chair.setFrameFromCenter(point,dia);
				
				ArrayList<Double> joints = new ArrayList<Double>();
				for(int k =0;k<jointnumber;k++){
					double angle = (r.nextInt(301) - 150)*Math.PI/180;
					joints.add(angle);
				}
				
				ArmConfig arm = new ArmConfig(point,joints,defaultGrip);
				
				if(testvalid(arm)){
					map.add(arm);
				}
			}
			map.add(goal);
		}else{
			Random  r = new Random();
			for(int i= 0; i<1000;i++){
				double x= r.nextDouble();
				double y= r.nextDouble();
				Point2D point = new Point2D.Double();
				point.setLocation(x, y);
				Point2D dia = new Point2D.Double();
				dia.setLocation(x+0.02, y+0.02);
				Rectangle2D chair = new Rectangle2D.Double();
				chair.setFrameFromCenter(point,dia);
				
				ArrayList<Double> joints = new ArrayList<Double>();
				for(int k =0;k<jointnumber;k++){
					double angle = (r.nextInt(301) - 150)*Math.PI/180;
					joints.add(angle);
				}
				
				ArmConfig arm = new ArmConfig(point,joints);
				
				if(testvalid(arm)){
					map.add(arm);
				}
			}
			map.add(goal);
		}
	}
	
	private boolean testvalid(ArmConfig arm){
		return test.fitsBounds(arm)&&!test.hasCollision(arm, obstacles)
				&&!test.hasSelfCollision(arm)&&test.hasValidJointAngles(arm);
	}
	
	
	public ArrayList<ArmConfig> search(){
		if(gripper){
			queue.add(new Node(initial,null,0.0));
			while(true){
				if(queue.peek().current.equals(goal)){
					Node a = new Node(queue.peek().current,queue.peek().parent,queue.peek().cost);
					while(!a.current.equals(initial)){
						for(int i =49;i>0;i--){
							path.add(0,cc(a.parent.current,a.current,i));
						}
						path.add(0,a.parent.current);
						
						a = a.parent;
					}
					path.add(goal);
					return path;
				}
				if(!expanded.contains(queue.peek().current)){
					expanded.add(queue.peek().current);
					for(int i =0; i<map.size();i++){
						if(checkLineVaild(map.get(i),queue.peek().current)){
							queue.add(new Node(map.get(i),queue.peek(),queue.peek().cost+he(map.get(i),queue.peek().current)));
							map.remove(i);
						}
					}
				}
				queue.remove();
			
			}	
		}else{
			///no gripper
			queue.add(new Node(initial,null,0.0));
			while(true){
				if(queue.peek().current.equals(goal)){
					Node a = new Node(queue.peek().current,queue.peek().parent,queue.peek().cost);
					while(!a.current.equals(initial)){
						for(int i =49;i>0;i--){
							path.add(0,cc(a.parent.current,a.current,i));
						}
						path.add(0,a.parent.current);
						
						a = a.parent;
					}
					path.add(goal);
					return path;
				}
				if(!expanded.contains(queue.peek().current)){
					expanded.add(queue.peek().current);
					for(int i =0; i<map.size();i++){
						if(checkLineVaild(map.get(i),queue.peek().current)){
							queue.add(new Node(map.get(i),queue.peek(),queue.peek().cost+he(map.get(i),queue.peek().current)));
							map.remove(i);
						}
					}
				}
				queue.remove();
			
			}	
		}
	}
	
	
	private ArrayList<ArmConfig> gripmove(ArmConfig a, ArmConfig b){
		ArrayList<ArmConfig> pmove = new ArrayList<ArmConfig>();
		ArrayList<Integer> vectors = new ArrayList<Integer>();
		
		if(b.getBaseCenter().getX()-a.getBaseCenter().getX()>0){
			vectors.add(0,1);
		}else if(b.getBaseCenter().getX()-a.getBaseCenter().getX()==0){
			vectors.add(0,0);
		}else{
			vectors.add(0,-1);
		}
		
		if(b.getBaseCenter().getY()-a.getBaseCenter().getY()>0){
			vectors.add(1,1);
		}else if(b.getBaseCenter().getY()-a.getBaseCenter().getY()==0){
			vectors.add(1,0);
		}else{
			vectors.add(1,-1);
		}
		
		for(int i = 0; i<a.getJointCount();i++){
			if(b.getJointAngles().get(i)-a.getJointAngles().get(i)>0){
				vectors.add(i+2,1);
			}else if(b.getJointAngles().get(i)-a.getJointAngles().get(i)==0){
				vectors.add(i+2,0);
			}else{
				vectors.add(i+2,-1);
			}
		}
		
		for(int i= 0;i<4;i++){
			if(b.getGripperLengths().get(i)-a.getGripperLengths().get(i)>0){
				vectors.add(i+a.getJointCount()+2,1);
			}else if(b.getGripperLengths().get(i)-a.getGripperLengths().get(i)==0){
				vectors.add(i+a.getJointCount()+2,0);
			}else{
				vectors.add(i+a.getJointCount()+2,-1);
			}
		}
		
		////
		ArmConfig c = new ArmConfig(a);
		Random r = new Random();
		
		while(true){
			//new x
			double x = r.nextDouble()*0.001;
			double y = Math.sqrt(0.001*0.001 - x*x);
			
			if(vectors.get(0)==1){
				x = c.getBaseCenter().getX()+x;
			}else if(vectors.get(0)==-1){
				x = c.getBaseCenter().getX()-x;
			}else{
				x = c.getBaseCenter().getX();
			}
			
			//new y
			
			
			if(vectors.get(1)==1){
				y = c.getBaseCenter().getY()+y;
			}else if(vectors.get(1)==-1){
				y = c.getBaseCenter().getY()-y;
			}else{
				y= c.getBaseCenter().getY();
			}
			Point2D point = new Point2D.Double();
			point.setLocation(x, y);
			
			//new angles
			ArrayList<Double> angles = new ArrayList<Double>();
			for(int i = 0; i<a.getJointCount();i++){
				double angle = r.nextDouble()*0.00174533;
				if(vectors.get(i+2)==1){
					angles.add(angle+c.getJointAngles().get(i));
				}else if(vectors.get(i+2)==-1){
					angles.add(c.getJointAngles().get(i)-angle);
				}else{
					angles.add(c.getJointAngles().get(i));
				}
			}
			
			//new grippers
			ArrayList<Double> grippers = new ArrayList<Double>();
			for(int i=0;i<4;i++){
				double grip = r.nextDouble()*0.001;
				if(vectors.get(jointnumber+2+i)==1){
					grippers.add(grip+c.getGripperLengths().get(i));
				}else if(vectors.get(jointnumber+2+i)==-1){
					grippers.add(c.getGripperLengths().get(i)-grip);
				}else{
					grippers.add(c.getGripperLengths().get(i));
				}
			}
			
			c = new ArmConfig(point,angles,grippers);
			pmove.add(c);
			
			if(Math.abs(c.getBaseCenter().getX()-b.getBaseCenter().getX())<0.001
					&&vectors.get(0)!=0){
				vectors.remove(0);
				vectors.add(0,0);
			}
			if(Math.abs(c.getBaseCenter().getY()-b.getBaseCenter().getY())<0.001
					&&vectors.get(1)!=0){
				vectors.remove(1);
				vectors.add(1,0);
			}
			
			for(int i= 0;i<c.getJointCount();i++){
				if(Math.abs(c.getJointAngles().get(i)-b.getJointAngles().get(i))<0.00175
						&&vectors.get(i+2)!=0){
					vectors.remove(i+2);
					vectors.add(i+2,0);
						
				}
			}
			for(int i= 0;i<4;i++){
				if(Math.abs(c.getGripperLengths().get(i)-b.getGripperLengths().get(i))<0.001
						&&vectors.get(i+2+c.getJointCount())!=0){
					vectors.remove(i+2+c.getJointCount());
					vectors.add(i+2+c.getJointCount(),0);
					
				}
			}
			
			boolean d =true;
			for(int i =0 ;i<vectors.size();i++){
				if(vectors.get(i)!=0){
					d=false;
				}
			}
			if(d==true){
				return pmove;
			}
		}
	}
	
	private ArrayList<ArmConfig> move(ArmConfig a, ArmConfig b){
		ArrayList<ArmConfig> pmove = new ArrayList<ArmConfig>();
		ArrayList<Integer> vectors = new ArrayList<Integer>();
		////
		if(b.getBaseCenter().getX()-a.getBaseCenter().getX()>0){
			vectors.add(0,1);
		}else if(b.getBaseCenter().getX()-a.getBaseCenter().getX()==0){
			vectors.add(0,0);
		}else{
			vectors.add(0,-1);
		}
		
		if(b.getBaseCenter().getY()-a.getBaseCenter().getY()>0){
			vectors.add(1,1);
		}else if(b.getBaseCenter().getY()-a.getBaseCenter().getY()==0){
			vectors.add(1,0);
		}else{
			vectors.add(1,-1);
		}
		
		
		for(int i = 0; i<a.getJointCount();i++){
			if(b.getJointAngles().get(i)-a.getJointAngles().get(i)>0){
				vectors.add(i+2,1);
			}else if(b.getJointAngles().get(i)-a.getJointAngles().get(i)==0){
				vectors.add(i+2,0);
			}else{
				vectors.add(i+2,-1);
			}
		}
	
		//
		ArmConfig c = new ArmConfig(a);
		Random r = new Random();
		while(true){
			//new x
			double x = r.nextDouble()*0.001;
			double y = Math.sqrt(0.001*0.001 - x*x);
			
			if(vectors.get(0)==1){
				x = c.getBaseCenter().getX()+x;
			}else if(vectors.get(0)==-1){
				x = c.getBaseCenter().getX()-x;
			}else{
				x = c.getBaseCenter().getX();
			}
			
			//new y
			
			
			if(vectors.get(1)==1){
				y = c.getBaseCenter().getY()+y;
			}else if(vectors.get(1)==-1){
				y = c.getBaseCenter().getY()-y;
			}else{
				y= c.getBaseCenter().getY();
			}
			Point2D point = new Point2D.Double();
			point.setLocation(x, y);
			
			//new angles
			ArrayList<Double> angles = new ArrayList<Double>();
			for(int i = 0; i<a.getJointCount();i++){
				double angle = r.nextDouble()*0.00174533;
				if(vectors.get(i+2)==1){
					angles.add(angle+c.getJointAngles().get(i));
				}else if(vectors.get(i+2)==-1){
					angles.add(c.getJointAngles().get(i)-angle);
				}else{
					angles.add(c.getJointAngles().get(i));
				}
			}
			c = new ArmConfig(point,angles);
			pmove.add(c);
			//System.out.println(c.toString());
			if(Math.abs(c.getBaseCenter().getX()-b.getBaseCenter().getX())<0.001
					&&vectors.get(0)!=0){
				vectors.remove(0);
				vectors.add(0,0);
			}
			if(Math.abs(c.getBaseCenter().getY()-b.getBaseCenter().getY())<0.001
					&&vectors.get(1)!=0){
				vectors.remove(1);
				vectors.add(1,0);
			}
			
			for(int i= 0;i<c.getJointCount();i++){
				if(Math.abs(c.getJointAngles().get(i)-b.getJointAngles().get(i))<0.00175
						&&vectors.get(i+2)!=0){
					vectors.remove(i+2);
					vectors.add(i+2,0);
						
				}
			}
			
			boolean d =true;
			for(int i =0 ;i<vectors.size();i++){
				if(vectors.get(i)!=0){
					d=false;
				}
			}
			if(d==true){
				return pmove;
			}
			
			
		}
		
	}
	
	private boolean checkLineVaild(ArmConfig a, ArmConfig b){
		for(int i =0; i< 49;i++){
			if(!testvalid(cc(a,b,i+1))){
				return false;
			}
		}
		return true;
	}
	
	private ArmConfig cc(ArmConfig a, ArmConfig b, int p){
		Point2D point = new Point2D.Double();
		point.setLocation(a.getBaseCenter().getX()+p*(b.getBaseCenter().getX()-a.getBaseCenter().getX()
				)/50,a.getBaseCenter().getY()+p*(b.getBaseCenter().getY()-a.getBaseCenter().getY()
				)/50);
		ArrayList<Double> joints = new ArrayList<Double>();
		for(int v= 0; v<a.getJointCount();v++){
			double c = a.getJointAngles().get(v)+p*(b.getJointAngles().get(v)- a.getJointAngles().get(v))/50;
			joints.add(c);
		}
		if(gripper){
			ArmConfig mid = new ArmConfig(point,joints,defaultGrip);
			return mid;
		}else{
			ArmConfig mid = new ArmConfig(point,joints);
			return mid;
		}
		
	}
	
	

	private static double he(ArmConfig current, ArmConfig subgoal){
	double result = 0;
	double x = current.getBaseCenter().getX() - subgoal.getBaseCenter().getX();
	double y = current.getBaseCenter().getY() - subgoal.getBaseCenter().getY();
	result = Math.pow(x, 2)+Math.pow(y, 2);
	for(int i = 0; i< current.getJointAngles().size();i++){
		 double a = current.getJointAngles().get(i)-subgoal.getJointAngles().get(i);
		 result += Math.pow(a, 2);
	}
	result = Math.sqrt(result);
	return result;
	}

	class Node implements Comparator<Node>
	{		
		public ArmConfig current;
		public Node parent;
		public double cost;
 
		public Node()
		{
		}
 
		public Node(ArmConfig current, Node parent, double cost)
		{
    	this.current = current;
        this.parent = parent;
        this.cost = cost;
       
    }
 
    @Override
    public int compare(Node node1, Node node2)
    {
        if (node1.cost< node2.cost)
            return -1;
        if (node1.cost> node2.cost)
            return 1;
        return 0;
    }
 
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Node)
        {
            Node node = (Node) obj;
            if (this.current.equals(node.current))
            {
                return true;
            }
        }
        return false;
    	}
    
	}

	public static void main(String[] args) throws IOException {
		ProblemSpec problem = new ProblemSpec();
		problem.loadProblem(args[0]);
		UniformSample s = new UniformSample(problem);
		if(!gripper){
			s.search();
			ArrayList<String> c = new  ArrayList<String>();
			for(int i =0;i<path.size()-1;i++){
				c.add(path.get(i).toString());
				ArrayList<ArmConfig> kk = s.move(path.get(i),path.get(i+1));
				for(int j= 0;j<kk.size();j++){
					c.add(kk.get(j).toString());
				};
			}
			c.add(path.get(path.size()-1).toString());
			c.add(0,String.valueOf(c.size()-1));
			ResultWriter.write(args[1], c);
		}else{
			s.search();
			ArrayList<String> c = new  ArrayList<String>();
			for(int i =0;i<path.size()-1;i++){
				c.add(path.get(i).toString());
				ArrayList<ArmConfig> kk = s.gripmove(path.get(i),path.get(i+1));
				for(int j= 0;j<kk.size();j++){
					c.add(kk.get(j).toString());
				};
			}
			c.add(path.get(path.size()-1).toString());
			c.add(0,String.valueOf(c.size()-1));
			ResultWriter.write(args[1], c);
		}
	}
}

